# IPython Rendering of Models from Odum, "Modeling for all Scales" #

This IPython notebook presents example models from the book "Modeling for all Scales" by Howard and Elisabeth Odum.

The original text presents the models using EXTEND and BASIC. EXTEND is a proprietary modelling tool now known as ExtendSim, with prices starting at $1k per seat. The book originated as a universtiy course text in the 1980's, when BASIC was available on every PC and very widely known. It seems a shame that the timeless content of the book could be obscured by this dated software technology. We hope that by using modern free open-source software, these versions of the models will make the book more accessible.

To run, you will need Python, IPython and IPython notebook and matplotlib.

Given this, in a linux or other 'nix shell, cd to the directory and enter =ipython notebook=.  The notebook should open in a web browser.

So far, only the first model TANK has been implemented.

### Contact ###
Greg O'Keefe, January 2016
gregokeefe (at) netspace.net.au